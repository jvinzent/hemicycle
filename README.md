Ce dépôt a servi a créer un hémicycle interactif lors des élections départementales de mars 2015. Une partie des fichiers est lié au squelette HTML5 http://html5up.net/

Les fichiers réellement importants pour la visualisation sont, en plus de l'index :

- cg13.js
- cg13.css

Le fichier mobile.js, censé permettre l'affichage d'un message en cas de consultation depuis un terminal mobile, ne semble pas fonctionnel.

**Tutoriel de référence (adapté à une carte) :** https://parall.ax/blog/view/2985/tutorial-creating-an-interactive-svg-map

**Etapes préparatoires :**

1 Génération de l'hémicycle SVG

http://tools.wmflabs.org/parliamentdiagram/parliamentinputform.html

2 Conversion en Raphael

http://readysetraphael.com/
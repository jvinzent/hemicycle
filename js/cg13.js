/* var div = document.getElementById("map");     
var width=div.offsetWidth;
original_width=360;
original_height=190;
scale_factor=width/original_width;
var height=original_height*scale_factor;                   
var transformation='S'+scale_factor+','+scale_factor+',0,0';   
var rsr=Raphael(div, width, height);
*/

var score_gauche = 24;
var score_droite = 32;
var score_FN = 2;
var attribues = score_gauche + score_droite + score_FN;
var vacants = 58 - attribues;

var dataelus = [
  {
    "name":"Arles",
    "elus":"Nicolas Koukas"
  },
  {
    "name":"Arles",
    "elus":"Aurore Raoux"
  },
  {
    "name":"Gardanne",
    "elus":"Claude Jorda (sortant)"
  },
  {
    "name":"Gardanne",
    "elus":"Rosy Inaudi"
  },
  {
    "name":"Martigues",
    "elus":"GÃ©rard Frau"
  },
  {
    "name":"Martigues",
    "elus":"Evelyne Santoru-Joly (sortante)"
  },
  {
    "name":"Istres",
    "elus":"RenÃ© Raimondi (sortant)"
  },
  {
    "name":"Istres",
    "elus":"Nicole Joulia"
  },
  {
    "name":"Salon Sud-Ouest",
    "elus":"FrÃ©dÃ©ric Vigouroux (sortant)"
  },
  {
    "name":"Salon Sud-Ouest",
    "elus":"Monique Trinquet"
  },
  {
    "name":"PÃ©lissanne",
    "elus":"Jacky GÃ©rard (sortant)"
  },
  {
    "name":"PÃ©lissanne",
    "elus":"HÃ©lÃ¨ne Gente-Seaglio (PS)"
  },
  {
    "name":"Marseille 3",
    "elus":"Henri Jibrayel (sortant)"
  },
  {
    "name":"Marseille 3",
    "elus":"Josette Sportiello (sortante)"
  },
  {
    "name":"Marseille 4",
    "elus":"Rebia Benarioua (sortant)"
  },
  {
    "name":"Marseille 4",
    "elus":"Anne Di Marino"
  },
  {
    "name":"Marseille 5",
    "elus":"Denis Rossi (sortant)"
  },
  {
    "name":"Marseille 5",
    "elus":"Haouaria Hadj-Chick"
  },
  {
    "name":"Marseille 6",
    "elus":"Christophe Masse (sortant)"
  },
  {
    "name":"Marseille 6",
    "elus":"GeneviÃ¨ve Tranchida"
  },
  {
    "name":"Marseille 1",
    "elus":"BenoÃ®t Payan"
  },
  {
    "name":"Marseille 1",
    "elus":"MichÃ¨le Rubirola"
  },
  {
    "name":"Marseille 2",
    "elus":"Jean-NoÃ«l GuÃ©rini (sortant)"
  },
  {
    "name":"Marseille 2",
    "elus":"Lisette Narducci (sortante)"
  },
  {
    "name":"Aix-en-Provence Sud-Ouest",
    "elus":"Jean-Marc Perrin"
  },
  {
    "name":"Aix-en-Provence Sud-Ouest",
    "elus":"Dany Brunet"
  },
  {
    "name":"Allauch",
    "elus":"Bruno Genzana (sortant)"
  },
  {
    "name":"Allauch",
    "elus":"VÃ©ronique Miquelly"
  },
  {
    "name":"Aubagne",
    "elus":"GÃ©rard Gazay"
  },
  {
    "name":"Aubagne",
    "elus":"Sylvia Barthelemy"
  },
  {
    "name":"ChÃ¢teaurenard",
    "elus":"Lucien Limousin (sortant)"
  },
  {
    "name":"ChÃ¢teaurenard",
    "elus":"Corine Chabaud"
  },
  {
    "name":"Marignane",
    "elus":"Eric Le DissÃ¨s (sortant)"
  },
  {
    "name":"Marignane",
    "elus":"ValÃ©rie Guarino "
  },
  {
    "name":"Vitrolles",
    "elus":"Richard MalliÃ©"
  },
  {
    "name":"Vitrolles",
    "elus":"Sandra Saloum (sortante)"
  },
  {
    "name":"Aix-en-Provence Nord-Est",
    "elus":"Jean-Pierre Bouvet (sortant)"
  },
  {
    "name":"Aix-en-Provence Nord-Est",
    "elus":"Brigitte Deveza"
  },
  {
    "name":"Salon Nord-Est",
    "elus":"Henri Pons "
  },
  {
    "name":"Salon Nord-Est",
    "elus":"Marie-Pierre Callet"
  },
  {
    "name":"Trets",
    "elus":"Patricia Saez"
  },
  {
    "name":"Trets",
    "elus":"Jean-Claude FÃ©raud "
  },
  {
    "name":"La Ciotat",
    "elus":"Patrick BorÃ© (sortant)"
  },
  {
    "name":"La Ciotat",
    "elus":"Danielle Milon"
  },
  {
    "name":"Marseille 9",
    "elus":"Didier RÃ©ault (sortant)"
  },
  {
    "name":"Marseille 9",
    "elus":"Laure-AgnÃ¨s Caradec "
  },
  {
    "name":"Marseille 7",
    "elus":"Maurice Rey (sortant)"
  },
  {
    "name":"Marseille 7",
    "elus":"Marine Pustorino (sortante)"
  },
  {
    "name":"Marseille 8",
    "elus":"Thierry Santelli "
  },
  {
    "name":"Marseille 8",
    "elus":"Sylvie Carrega"
  },
  {
    "name":"Marseille 10",
    "elus":"Martine Vassal (sortant)"
  },
  {
    "name":"Marseille 10",
    "elus":"Lionel Royer-Perreaut"
  },
  {
    "name":"Marseille 11",
    "elus":"Maurice Di Nocera (sortant)"
  },
  {
    "name":"Marseille 11",
    "elus":"Solange Biaggi (sortante)"
  },
  {
    "name":"Marseille 12",
    "elus":"Yves Moraine"
  },
  {
    "name":"Marseille 12",
    "elus":"Sabine Bernasconi (sortante)"
  },
  {
    "name":"Berre",
    "elus":"Jean-Marie Verani"
  },
  {
    "name":"Berre",
    "elus":"Christiane Pujol"
  }
]
var rsr = Raphael('map', '661.20959', '331.27679');

var assemblee = rsr.set();

// 1 Ã  5

var circle9 = rsr.circle(17, 170, 10).attr({id: 'circle9',d: 'm 27.809999,170.00999 c 0,5.52285 -4.477152,10 -10,10 -5.522847,0 -9.9999995,-4.47715 -9.9999995,-10 0,-5.52284 4.4771525,-10 9.9999995,-10 5.522848,0 10,4.47716 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id':'circle9', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle11 = rsr.circle(42, 170, 10).attr({id: 'circle11',d: 'm 52.860001,170.02 c 0,5.52285 -4.477153,10 -10,10 -5.522848,0 -10,-4.47715 -10,-10 0,-5.52284 4.477152,-10 10,-10 5.522847,0 10,4.47716 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle11', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle13 = rsr.circle(67, 170, 10).attr({id: 'circle13',d: 'm 77.940002,170.03 c 0,5.52285 -4.477152,10 -10,10 -5.522847,0 -10,-4.47715 -10,-10 0,-5.52285 4.477153,-10 10,-10 5.522848,0 10,4.47715 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle13', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle15 = rsr.circle(93, 170, 10).attr({id: 'circle15',d: 'm 103.07,170.03999 c 0,5.52285 -4.477153,10 -10,10 -5.522848,0 -10,-4.47715 -10,-10 0,-5.52284 4.477152,-10 10,-10 5.522847,0 10,4.47716 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle15', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle17 = rsr.circle(21, 143, 10).attr({id: 'circle17',d: 'm 31.75,143.08 c 0,5.52285 -4.477153,10 -10,10 -5.522847,0 -10,-4.47715 -10,-10 0,-5.52285 4.477153,-10 10,-10 5.522847,0 10,4.47715 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle17', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 6 Ã  10

var circle19 = rsr.circle(47, 143, 10).attr({id: 'circle19',d: 'm 57.57,143 c 0,5.52285 -4.477153,10 -10,10 -5.522848,0 -10,-4.47715 -10,-10 0,-5.52285 4.477152,-10 10,-10 5.522847,0 10,4.47715 10,10 z',parent: 'g4',fill: '#d35f68','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle19', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle22 = rsr.circle(73, 142, 10).attr({id: 'circle22',d: 'm 83.779999,142.92999 c 0,5.52285 -4.477153,10 -10,10 -5.522848,0 -10,-4.47715 -10,-10 0,-5.52284 4.477152,-10 10,-10 5.522847,0 10,4.47716 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle22', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle24 = rsr.circle(30, 117, 10).attr({id: 'circle24',d: 'm 40.129999,117.18 c 0,5.52285 -4.477152,10 -10,10 -5.522847,0 -10,-4.47715 -10,-10 0,-5.52285 4.477153,-10 10,-10 5.522848,0 10,4.47715 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle24', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle26 = rsr.circle(100, 142, 10).attr({id: 'circle26',d: 'm 110.75,142.91 c 0,5.52285 -4.47715,10 -10,10 -5.522847,0 -10,-4.47715 -10,-10 0,-5.52284 4.477153,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle26', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle28 = rsr.circle(57, 117, 10).attr({id: 'circle28',d: 'm 67.540001,117.46 c 0,5.52285 -4.477153,10 -10,10 -5.522848,0 -10,-4.47715 -10,-10 0,-5.52285 4.477152,-10 10,-10 5.522847,0 10,4.47715 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle28', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 11 Ã  15

var circle30 = rsr.circle(42, 93, 10).attr({id: 'circle30',d: 'm 52.720001,93.050003 c 0,5.522848 -4.477152,9.999997 -10,9.999997 -5.522847,0 -10,-4.477149 -10,-9.999997 0,-5.522847 4.477153,-10 10,-10 5.522848,0 10,4.477153 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle30', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle32 = rsr.circle(86, 118, 10).attr({id: 'circle32',d: 'm 96.07,118.09 c 0,5.52284 -4.477153,10 -10,10 -5.522848,0 -10,-4.47716 -10,-10 0,-5.52285 4.477152,-10 10,-10 5.522847,0 10,4.47715 10,10 z',parent: 'g4',fill: '#0800ff','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle32', 'name': 'canton','elus':'M. 1 et Mme 2'});


var circle35 = rsr.circle(72, 94, 10).attr({id: 'circle35',d: 'm 82.389999,94.410004 c 0,5.522847 -4.477152,9.999996 -10,9.999996 -5.522847,0 -10,-4.477149 -10,-9.999996 0,-5.522848 4.477153,-10 10,-10 5.522848,0 10,4.477152 10,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle35', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle37 = rsr.circle(59, 71, 10).attr({id: 'circle37',d: 'm 69.16,71.360001 c 0,5.522847 -4.477153,10 -10,10 -5.522848,0 -10,-4.477153 -10,-10 0,-5.522848 4.477152,-10 10,-10 5.522847,0 10,4.477152 10,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle37', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle39 = rsr.circle(116, 119, 10).attr({id: 'circle39',d: 'm 126.67,119.63 c 0,5.52284 -4.47715,10 -10,10 -5.52285,0 -10,-4.47716 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle39', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 16 Ã  20

var circle41 = rsr.circle(104, 97, 10).attr({id: 'circle41',d: 'm 114.06,97 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10.000002,-4.47715 -10.000002,-10 0,-5.522847 4.477152,-10 10.000002,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle41', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle43 = rsr.circle(91, 74, 10).attr({id: 'circle43',d: 'm 101.51,74.760002 c 0,5.522848 -4.47715,10 -9.999998,10 -5.522847,0 -10,-4.477152 -10,-10 0,-5.522847 4.477153,-10 10,-10 5.522848,0 9.999998,4.477153 9.999998,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle43', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle45 = rsr.circle(78, 52, 10).attr({id: 'circle45',d: 'm 88.989998,52.709999 c 0,5.522848 -4.477153,10 -10,10 -5.522848,0 -10,-4.477152 -10,-10 0,-5.522847 4.477152,-10 10,-10 5.522847,0 10,4.477153 10,10 z',parent: 'g4',fill: '#4e004e','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle45', 'name': 'canton','elus':'M. 1 et Mme 2'});


var circle48 = rsr.circle(101, 37, 10).attr({id: 'circle48',d: 'm 111.65,37.630001 c 0,5.522848 -4.47715,10 -10,10 -5.522846,0 -9.999998,-4.477152 -9.999998,-10 0,-5.522847 4.477152,-10 9.999998,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle48', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle50 = rsr.circle(114, 59, 10).attr({id: 'circle50',d: 'm 124.15,59.290001 c 0,5.522847 -4.47715,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52285,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle50', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 21 Ã  25

var circle52 = rsr.circle(126, 80, 10).attr({id: 'circle52',d: 'm 136.66,80.949997 c 0,5.522847 -4.47715,10 -10,10 -5.52284,0 -10,-4.477153 -10,-10 0,-5.522848 4.47716,-10 10,-10 5.52285,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle52', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle54 = rsr.circle(139, 102, 10).attr({id: 'circle54',d: 'm 149.16,102.61 c 0,5.52285 -4.47715,10 -10,10 -5.52284,0 -10,-4.47715 -10,-10 0,-5.522847 4.47716,-9.999999 10,-9.999999 5.52285,0 10,4.477152 10,9.999999 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle54', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle56 = rsr.circle(126, 26, 10).attr({id: 'circle56',d: 'm 136.51,26.549999 c 0,5.522848 -4.47715,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle56', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle58 = rsr.circle(139, 48, 10).attr({id: 'circle58',d: 'm 149.42,48.630001 c 0,5.522848 -4.47715,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle58', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle60 = rsr.circle(152, 70, 10).attr({id: 'circle60',d: 'm 162.49001,70.910004 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle60', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 26 Ã  30

var circle62 = rsr.circle(152, 19, 10).attr({id: 'circle62',d: 'm 162.88,19.780001 c 0,5.522847 -4.47715,10 -10,10 -5.52284,0 -10,-4.477153 -10,-10 0,-5.522848 4.47716,-10.0000003 10,-10.0000003 5.52285,0 10,4.4771523 10,10.0000003 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle62', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle64 = rsr.circle(165, 93, 10).attr({id: 'circle64',d: 'm 175.89999,93.639999 c 0,5.522848 -4.47715,10.000001 -10,10.000001 -5.52284,0 -10,-4.477153 -10,-10.000001 0,-5.522847 4.47716,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle64', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle66 = rsr.circle(166, 43, 10).attr({id: 'circle66',d: 'm 176.28999,43.189999 c 0,5.522847 -4.47715,10 -10,10 -5.52284,0 -10,-4.477153 -10,-10 0,-5.522848 4.47716,-10 10,-10 5.52285,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle66', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle68 = rsr.circle(180, 67, 10).attr({id: 'circle68',d: 'm 190,67.5 c 0,5.522847 -4.47715,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle68', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle70 = rsr.circle(180, 17, 10).attr({id: 'circle70',d: 'm 190,17.5 c 0,5.522847 -4.47715,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle70', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 31 Ã  35

var circle72 = rsr.circle(193, 43, 10).attr({id: 'circle72',d: 'm 203.71001,43.189999 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle72', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle74 = rsr.circle(194, 93, 10).attr({id: 'circle74',d: 'm 204.10001,93.639999 c 0,5.522848 -4.47716,10.000001 -10,10.000001 -5.52285,0 -10,-4.477153 -10,-10.000001 0,-5.522847 4.47715,-10 10,-10 5.52284,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle74', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle76 = rsr.circle(207, 19, 10).attr({id: 'circle76',d: 'm 217.12,19.780001 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10.0000003 10,-10.0000003 5.52284,0 10,4.4771523 10,10.0000003 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle76', 'name': 'canton','elus':'M. 1 et Mme 2'});
var circle78 = rsr.circle(207, 70, 10).attr({id: 'circle78',d: 'm 217.50999,70.910004 c 0,5.522847 -4.47715,10 -10,10 -5.52284,0 -10,-4.477153 -10,-10 0,-5.522848 4.47716,-10 10,-10 5.52285,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle78', 'name': 'canton','elus':'M. 1 et Mme 2'});
var circle80 = rsr.circle(220, 48, 10).attr({id: 'circle80',d: 'm 230.58,48.630001 c 0,5.522848 -4.47715,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle80', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 36 Ã  40

var circle82 = rsr.circle(233, 26, 10).attr({id: 'circle82',d: 'm 243.49001,26.549999 c 0,5.522848 -4.47716,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52284,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle82', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle84 = rsr.circle(220, 102, 10).attr({id: 'circle84',d: 'm 230.84,102.61 c 0,5.52285 -4.47716,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.522847 4.47715,-9.999999 10,-9.999999 5.52284,0 10,4.477152 10,9.999999 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle84', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle86 = rsr.circle(233, 80, 10).attr({id: 'circle86',d: 'm 243.34,80.949997 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle86', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle88 = rsr.circle(245, 59, 10).attr({id: 'circle88',d: 'm 255.85001,59.290001 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle88', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle90 = rsr.circle(258, 37, 10).attr({id: 'circle90',d: 'm 268.35001,37.630001 c 0,5.522848 -4.47716,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52284,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle90', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 41 Ã  45

var circle92 = rsr.circle(281, 52, 10).attr({id: 'circle92',d: 'm 291.01001,52.709999 c 0,5.522848 -4.47715,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle92', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle94 = rsr.circle(268, 74, 10).attr({id: 'circle94',d: 'm 278.48999,74.760002 c 0,5.522848 -4.47715,10 -10,10 -5.52285,0 -10,-4.477152 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle94', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle96 = rsr.circle(255, 97, 10).attr({id: 'circle96',d: 'm 265.94,97 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle96', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle98 = rsr.circle(243, 119, 10).attr({id: 'circle98',d: 'm 253.33,119.63 c 0,5.52284 -4.47715,10 -10,10 -5.52285,0 -10,-4.47716 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle98', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle100 = rsr.circle(300, 71, 10).attr({id: 'circle100',d: 'm 310.84,71.360001 c 0,5.522847 -4.47716,10 -10,10 -5.52285,0 -10,-4.477153 -10,-10 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle100', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 46 Ã  50

var circle102 = rsr.circle(287, 94, 10).attr({id: 'circle102',d: 'm 297.60999,94.410004 c 0,5.522847 -4.47716,9.999996 -10,9.999996 -5.52285,0 -10,-4.477149 -10,-9.999996 0,-5.522848 4.47715,-10 10,-10 5.52284,0 10,4.477152 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle102', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle104 = rsr.circle(273, 118, 10).attr({id: 'circle104',d: 'm 283.92999,118.09 c 0,5.52284 -4.47715,10 -10,10 -5.52284,0 -10,-4.47716 -10,-10 0,-5.52285 4.47716,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle104', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle106 = rsr.circle(317, 93, 10).attr({id: 'circle106',d: 'm 327.28,93.050003 c 0,5.522848 -4.47715,9.999997 -10,9.999997 -5.52285,0 -10,-4.477149 -10,-9.999997 0,-5.522847 4.47715,-10 10,-10 5.52285,0 10,4.477153 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle106', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle108 = rsr.circle(302, 117, 10).attr({id: 'circle108',d: 'm 312.45999,117.46 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle108', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle110 = rsr.circle(259, 142, 10).attr({id: 'circle110',d: 'm 269.25,142.91 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52284 4.47715,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle110', 'name': 'canton','elus':'M. 1 et Mme 2'});

// 51 Ã  58

var circle112 = rsr.circle(329, 117, 10).attr({id: 'circle112',d: 'm 339.87,117.18 c 0,5.52285 -4.47716,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52284,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle112', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle114 = rsr.circle(286, 142, 10).attr({id: 'circle114',d: 'm 296.22,142.92999 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52284 4.47715,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle114', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle116 = rsr.circle(312, 143, 10).attr({id: 'circle116',d: 'm 322.42999,143 c 0,5.52285 -4.47715,10 -10,10 -5.52284,0 -10,-4.47715 -10,-10 0,-5.52285 4.47716,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle116', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle118 = rsr.circle(338, 143, 10).attr({id: 'circle118',d: 'm 348.25,143.08 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle118', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle120 = rsr.circle(266, 170, 10).attr({id: 'circle120',d: 'm 276.92999,170.03999 c 0,5.52285 -4.47715,10 -10,10 -5.52284,0 -10,-4.47715 -10,-10 0,-5.52284 4.47716,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle120', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle122 = rsr.circle(292, 170, 10).attr({id: 'circle122',d: 'm 302.06,170.03 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52285 4.47715,-10 10,-10 5.52285,0 10,4.47715 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle122', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle124 = rsr.circle(317, 170, 10).attr({id: 'circle124',d: 'm 327.14001,170.02 c 0,5.52285 -4.47715,10 -10,10 -5.52284,0 -10,-4.47715 -10,-10 0,-5.52284 4.47716,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle124', 'name': 'canton','elus':'M. 1 et Mme 2'});

var circle126 = rsr.circle(342, 170, 10).attr({id: 'circle126',d: 'm 352.19,170.00999 c 0,5.52285 -4.47715,10 -10,10 -5.52285,0 -10,-4.47715 -10,-10 0,-5.52284 4.47715,-10 10,-10 5.52285,0 10,4.47716 10,10 z',parent: 'g4',fill: '#ececec','stroke-width': '0','stroke-opacity': '1'}).transform("m1.92,0,0,1.92,-14.995199,-14.4").data({'id': 'circle126', 'name': 'canton','elus':'M. 1 et Mme 2'});

assemblee.push(
	circle9 ,
	circle11 ,
	circle13 ,
	circle15 ,
	circle17 ,
	circle19,
	circle22 ,
	circle24 ,
	circle26 ,
	circle28 ,
	circle30 ,
	circle32,
circle35 ,
	circle37 ,
	circle39 ,
	circle41 ,
	circle43 ,
	circle45 ,
circle48 ,
	circle50 ,
	circle52 ,
	circle54 ,
	circle56 ,
	circle58 ,
	circle60 ,
	circle62 ,
	circle64 ,
	circle66 ,
	circle68 ,
	circle70 ,
	circle72 ,
	circle74 ,
	circle76 ,
	circle78 ,
	circle80 ,
	circle82 ,
	circle84 ,
	circle86 ,
	circle88 ,
	circle90 ,
	circle92 ,
	circle94 ,
	circle96 ,
	circle98 ,
	circle100 ,
	circle102 ,
	circle104 ,
	circle106 ,
	circle108 ,
	circle110 ,
	circle112 ,
	circle114 ,
	circle116 ,
	circle118 ,
	circle120 ,
	circle122 ,
	circle124 ,
	circle126 
);

// Attribution des couleurs en fonction des rÃ©sultats

// Gauche
for (var i = 0; i < score_gauche; i++) {
assemblee[i].node.setAttribute('fill', '#d35f68');
assemblee[i].data(dataelus[i]);
if (assemblee[i].data("elus") == "Rosy Inaudi") {assemblee[i].node.setAttribute('fill', '#147d00');};
if (assemblee[i].data("elus") == "MichÃ¨le Rubirola") {assemblee[i].node.setAttribute('fill', '#147d00');};
if (assemblee[i].data("elus") == "Claude Jorda (sortant)") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "FrÃ©dÃ©ric Rays") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "Nicolas Koukas") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "Aurore Raoux") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "GÃ©rard Frau") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "Evelyne Santoru-Joly (sortante)") {assemblee[i].node.setAttribute('fill', '#b0001c');};
if (assemblee[i].data("elus") == "Jean-NoÃ«l GuÃ©rini (sortant)") {assemblee[i].node.setAttribute('fill', '#eed153');};
if (assemblee[i].data("elus") == "Lisette Narducci (sortante)") {assemblee[i].node.setAttribute('fill', '#eed153');};

// Infobas
assemblee[i].mouseover(function(e){
this.node.style.opacity = 0.7;
document.getElementById('nom-elu').innerHTML = "<h2>" + score_gauche + " si&egrave;ges pour la gauche" + "</h2>" + "<div class='infobulle'>Canton de <b>" + this.data("name") +  "</b><br>" + this.data("elus") + "</div><div class='note'><em>Majorit&eacute; absolue &agrave; 30 si&egrave;ges</em></div>";
});
assemblee[i].mouseout(function(e){
this.node.style.opacity = 1;
document.getElementById('nom-elu').innerHTML = "";
});
}

// Droite
for (var i = score_gauche; i < score_gauche + score_droite; i++) {
assemblee[i].node.setAttribute('fill', '#064070');
assemblee[i].data(dataelus[i]);

// Infobas
assemblee[i].mouseover(function(e){
this.node.style.opacity = 0.7;
document.getElementById('nom-elu').innerHTML = "<h2>" + score_droite + " si&egrave;ges pour la droite" + "</h2>" + "<div class='infobulle'>Canton de <b>" + this.data("name") +  "</b><br>" + this.data("elus") + "</div><div class='note'><em>Majorit&eacute; absolue &agrave; 30 si&egrave;ges</em></div>";
});
assemblee[i].mouseout(function(e){
this.node.style.opacity = 1;
document.getElementById('nom-elu').innerHTML = "";
}); 

}

// FN
for (var i = score_gauche + score_droite ; i < score_gauche + score_droite + score_FN; i++){
assemblee[i].node.setAttribute('fill', '#4d004d');
assemblee[i].data(dataelus[i]);

// Infobas
assemblee[i].mouseover(function(e){
this.node.style.opacity = 0.7;
document.getElementById('nom-elu').innerHTML = "<h2>" + score_FN + " si&egrave;ges pour le FN" + "</h2>" + "<div class='infobulle'>Canton de <b>" + this.data("name") +  "</b><br>" + this.data("elus") + "</div><div class='note'><em>Majorit&eacute; absolue &agrave; 30 si&egrave;ges</em></div>";
});
assemblee[i].mouseout(function(e){
this.node.style.opacity = 1;
document.getElementById('nom-elu').innerHTML = "";
});
}

// Attente
for (var i = 58 - vacants; i < assemblee.length; i++) {
assemblee[i].node.setAttribute('fill', '#ececec');
// Infobas
assemblee[i].mouseover(function(e){
this.node.style.opacity = 0.7;
document.getElementById('nom-elu').innerHTML = "<h2>" + vacants + " si&egrave;ges en attente de r&eacute;sultats" + "<h2>";
});
assemblee[i].mouseout(function(e){
this.node.style.opacity = 1;
document.getElementById('nom-elu').innerHTML = "";
});
}